/// <reference path='_all.ts' />

/**
 * The main TodoMVC app module.
 *
 * @type {angular.Module}
 */
module summer {
    'use strict';

    var summer = angular.module('summer', [])
        .controller('productCtrl', ProductCtrl.prototype.injection())
        .directive('productBlur', ProductBlur.prototype.injection())
        .directive('productFocus', ProductFocus.prototype.injection())
        .service('summerStorage', SummerStorage.prototype.injection());
}