var summer;
(function (summer) {
    'use strict';

    var SummerStorage = (function () {
        function SummerStorage() {
        }
        SummerStorage.prototype.injection = function () {
            return [
                SummerStorage
            ];
        };

        SummerStorage.prototype.get = function (resetProducts) {
            $.ajax({
                type: 'GET',
                url: '/app/index.php/products',
                contentType: 'application/json',
                success: function (response) {
                    resetProducts(response);
                }
            });
        };

        SummerStorage.prototype.add = function (product, resetProducts) {
            $.ajax({
                type: 'POST',
                url: '/app/index.php/product',
                contentType: 'application/json',
                data: JSON.stringify(product),
                success: function (response) {
                    resetProducts(response);
                }
            });
        };

        SummerStorage.prototype.edit = function (product, resetProducts) {
            $.ajax({
                type: 'POST',
                url: '/app/index.php/product/' + product.id,
                contentType: 'application/json',
                data: JSON.stringify(product),
                success: function (response) {
                    resetProducts(response);
                }
            });
        };

        SummerStorage.prototype.remove = function (product, resetProducts) {
            $.ajax({
                type: 'POST',
                url: '/app/index.php/product/' + product.id + '/delete',
                contentType: 'application/json',
                data: JSON.stringify(product),
                success: function (response) {
                    resetProducts(response);
                }
            });
        };
        return SummerStorage;
    })();
    summer.SummerStorage = SummerStorage;
})(summer || (summer = {}));
//@ sourceMappingURL=SummerStorage.js.map
