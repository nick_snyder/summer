/// <reference path='../_all.ts' />

module summer {
    'use strict';

    /**
     * Services that persists and retrieves TODOs from localStorage.
     */
    export class SummerStorage implements ISummerStorage {

        public injection(): any[] {
            return [
                SummerStorage
            ]
        }

        constructor() {
        }

        get (resetProducts: Function): void {
            $.ajax(
                {
                    type: 'GET',
                    url: '/app/index.php/products',
                    contentType: 'application/json',
                    success: function(response){
                        resetProducts(response);
                    }
                }
            );
        }

        add(product: Product, resetProducts: Function) {

            $.ajax(
                {
                    type: 'POST',
                    url: '/app/index.php/product',
                    contentType: 'application/json',
                    data: JSON.stringify(product),
                    success: function(response){
                        resetProducts(response);
                    }
                }
            );
        }

        edit(product: Product, resetProducts: Function) {
            $.ajax(
                {
                    type: 'POST',
                    url: '/app/index.php/product/' + product.id,
                    contentType: 'application/json',
                    data: JSON.stringify(product),
                    success: function(response){
                        resetProducts(response);
                    }
                }
            );
        }

        remove(product: Product, resetProducts: Function) {
            $.ajax(
                {
                    type: 'POST',
                    url: '/app/index.php/product/' + product.id + '/delete',
                    contentType: 'application/json',
                    data: JSON.stringify(product),
                    success: function(response){
                        resetProducts(response);
                    }
                }
            );
        }
    }
}