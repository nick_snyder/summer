/// <reference path="../_all.ts" />

module summer {
    'use strict';

    export class ProductCtrl {

        private products: Product[];

        resetProducts: (products: Product[]) => void;


        public injection(): any[] {
            return [
                '$scope',
                '$location',
                'summerStorage',
                'filterFilter',
                ProductCtrl
            ]
        }

        constructor(
            private $scope: ISummerScope,
            private $location: ng.ILocationService,
            private summerStorage: ISummerStorage,
            private filterFilter
            ) {

            this.resetProducts = (products: Product[]) => {
                this.products = products;
            }

            summerStorage.get(this.resetProducts);

            $scope.productName = '';
            $scope.productDescription = '';
            $scope.productPrice = 0;
            $scope.editedProduct = null;

            $scope.addProduct = () => this.addProduct();
            $scope.editProduct = (product) => this.editProduct(product);
            $scope.doneEditing = (product) => this.doneEditing(product);
            $scope.removeProduct = (product) => this.removeProduct(product);

            $scope.$watch('location.path()', (path) => this.onPath(path));

            if($location.path() === '') $location.path('/');
            $scope.location = $location;


        }

        onPath(path: string) {
            this.$scope.statusFilter =
                (path == '/active')
                    ? { completed: false }
                    : ( path == '/completed' )
                        ? { completed: true }
                        : null;
        }

        addProduct() {
            if(!this.$scope.productName.length) {
                return;
            }

            this.summerStorage.add(new Product(null, this.$scope.productName, this.$scope.productDescription, this.$scope.productPrice), this.resetProducts);
            this.$scope.productName = '';
            this.$scope.productDescription = '';
            this.$scope.productPrice = 0;
        }

        editProduct(product: Product) {
            this.$scope.editedProduct = product;
        }

        doneEditing(product: Product) {
            this.$scope.editedProduct = null;

            if(!product.name) {
                this.$scope.removeProduct(product);
            }
        }

        removeProduct(product: Product) {
            this.products.splice(this.products.indexOf(product), 1);
        }

    }
}