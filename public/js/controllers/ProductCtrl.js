var summer;
(function (summer) {
    'use strict';

    var ProductCtrl = (function () {
        function ProductCtrl($scope, $location, summerStorage, filterFilter) {
            var _this = this;
            this.$scope = $scope;
            this.$location = $location;
            this.summerStorage = summerStorage;
            this.filterFilter = filterFilter;
            this.resetProducts = function (products) {
                _this.products = products;
            };

            summerStorage.get(this.resetProducts);

            $scope.productName = '';
            $scope.productDescription = '';
            $scope.productPrice = 0;
            $scope.editedProduct = null;

            $scope.addProduct = function () {
                return _this.addProduct();
            };
            $scope.editProduct = function (product) {
                return _this.editProduct(product);
            };
            $scope.doneEditing = function (product) {
                return _this.doneEditing(product);
            };
            $scope.removeProduct = function (product) {
                return _this.removeProduct(product);
            };

            $scope.$watch('location.path()', function (path) {
                return _this.onPath(path);
            });

            if ($location.path() === '')
                $location.path('/');
            $scope.location = $location;
        }
        ProductCtrl.prototype.injection = function () {
            return [
                '$scope',
                '$location',
                'summerStorage',
                'filterFilter',
                ProductCtrl
            ];
        };

        ProductCtrl.prototype.onPath = function (path) {
            this.$scope.statusFilter = (path == '/active') ? { completed: false } : (path == '/completed') ? { completed: true } : null;
        };

        ProductCtrl.prototype.addProduct = function () {
            if (!this.$scope.productName.length) {
                return;
            }

            this.summerStorage.add(new summer.Product(null, this.$scope.productName, this.$scope.productDescription, this.$scope.productPrice), this.resetProducts);
            this.$scope.productName = '';
            this.$scope.productDescription = '';
            this.$scope.productPrice = 0;
        };

        ProductCtrl.prototype.editProduct = function (product) {
            this.$scope.editedProduct = product;
        };

        ProductCtrl.prototype.doneEditing = function (product) {
            this.$scope.editedProduct = null;

            if (!product.name) {
                this.$scope.removeProduct(product);
            }
        };

        ProductCtrl.prototype.removeProduct = function (product) {
            this.products.splice(this.products.indexOf(product), 1);
        };
        return ProductCtrl;
    })();
    summer.ProductCtrl = ProductCtrl;
})(summer || (summer = {}));
//@ sourceMappingURL=ProductCtrl.js.map
