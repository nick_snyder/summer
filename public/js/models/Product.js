var summer;
(function (summer) {
    'use strict';

    var Product = (function () {
        function Product(id, name, description, price) {
            this.id = id;
            this.name = name;
            this.description = description;
            this.price = price;
        }
        return Product;
    })();
    summer.Product = Product;
})(summer || (summer = {}));
//@ sourceMappingURL=Product.js.map
