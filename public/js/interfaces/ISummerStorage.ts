/// <reference path='../_all.ts' />

module summer {
    'use strict';

    export interface ISummerStorage {
        get (resetProducts: Function): void;
        add(product: Product, resetProducts: Function);
        edit(product: Product, resetProducts: Function);
        remove(product:Product, resetProducts: Function);
    }
}