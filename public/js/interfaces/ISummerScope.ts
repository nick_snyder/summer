/// <reference path='../_all.ts' />

module summer {

    'use strict';

    export interface ISummerScope extends ng.IScope {
        products: Product[];
        productName: string;
        productDescription: string;
        productPrice: number;
        editedProduct: Product;
        remainingCount: number;
        doneCount: number;
        allChecked: bool;
        statusFilter: { completed: bool; };
        location: ng.ILocationService;

        addProduct: () => void;
        editProduct: (product: Product) => void;
        doneEditing: (product: Product) => void;
        removeProduct: (product: Product) => void;
    }
}