var summer;
(function (summer) {
    'use strict';

    var summer = angular.module('summer', []).controller('productCtrl', summer.ProductCtrl.prototype.injection()).directive('productBlur', summer.ProductBlur.prototype.injection()).directive('productFocus', summer.ProductFocus.prototype.injection()).service('summerStorage', summer.SummerStorage.prototype.injection());
})(summer || (summer = {}));
//@ sourceMappingURL=Application.js.map
