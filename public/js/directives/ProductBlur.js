var summer;
(function (summer) {
    'use strict';

    var ProductBlur = (function () {
        function ProductBlur() {
            var _this = this;
            this.link = function ($scope, element, attributes) {
                return _this.linkFn($scope, element, attributes);
            };
        }
        ProductBlur.prototype.injection = function () {
            return [
                function () {
                    return new ProductBlur();
                }
            ];
        };

        ProductBlur.prototype.linkFn = function ($scope, element, attributes) {
            element.bind('blur', function () {
                $scope.$apply(attributes.productBlur);
            });
        };
        return ProductBlur;
    })();
    summer.ProductBlur = ProductBlur;
})(summer || (summer = {}));
//@ sourceMappingURL=ProductBlur.js.map
