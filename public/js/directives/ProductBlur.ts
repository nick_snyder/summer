/// <reference path='../_all.ts' />

module summer {
    'use strict';

    export class ProductBlur {
        public link: ($scope: ng.IScope, element: JQuery, attributes: any) => any;

        public injection(): any[] {
            return [
                () => { return new ProductBlur(); }
            ]
        }

        constructor() {
            this.link = ($scope, element, attributes) => this.linkFn($scope, element, attributes);
        }

        linkFn($scope: ng.IScope, element: JQuery, attributes: any): any {
            element.bind('blur', () => {
                $scope.$apply(attributes.productBlur);
            });
        }
    }
}