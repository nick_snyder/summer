/// <reference path='../_all.ts'/>

module summer {
    'use strict';

    export class ProductFocus {
        public link: ($scope: ng.IScope, element: JQuery, attributes: any) => any;

        public injection(): any[] {
            return [
            '$timeout',
                ($timeout) => {return new ProductFocus($timeout);}
            ]
        }

        constructor(private $timeout: ng.ITimeoutService){
            this.link = ($scope, element, attributes) => this.linkFn($scope, element, attributes);
        }

        linkFn($scope: ng.IScope, element: JQuery, attributes: any): any {
            $scope.$watch(attributes.productFocus, (newval) =>{
                if(newval){
                    this.$timeout(() => {
                        element[0].focus();
                    }, 0, false);
                }
            });
        }
    }
}