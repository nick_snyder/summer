var summer;
(function (summer) {
    'use strict';

    var ProductFocus = (function () {
        function ProductFocus($timeout) {
            var _this = this;
            this.$timeout = $timeout;
            this.link = function ($scope, element, attributes) {
                return _this.linkFn($scope, element, attributes);
            };
        }
        ProductFocus.prototype.injection = function () {
            return [
                '$timeout',
                function ($timeout) {
                    return new ProductFocus($timeout);
                }
            ];
        };

        ProductFocus.prototype.linkFn = function ($scope, element, attributes) {
            var _this = this;
            $scope.$watch(attributes.productFocus, function (newval) {
                if (newval) {
                    _this.$timeout(function () {
                        element[0].focus();
                    }, 0, false);
                }
            });
        };
        return ProductFocus;
    })();
    summer.ProductFocus = ProductFocus;
})(summer || (summer = {}));
//@ sourceMappingURL=ProductFocus.js.map
