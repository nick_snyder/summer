<!doctype html>
<html lang="en" ng-app="summer" data-framework="typescript">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <title>Summer</title>
    <style>[ng-cloak] { display: none; }</style>
    <link rel="stylesheet" href="css/site.css"/>
</head>
<body>
<section id="summerapp" ng-controller="productCtrl">
    <header id="header">
        <h1>products</h1>
        <form id="product-form" ng-submit="addProduct()">
            <input id="new-product-name" placeholder="Product Name" ng-model="productName" autofocus>
            <textarea name="new-product-description" id="new-product-description" ng-model="productDescription" cols="30" rows="10" placeholder="Product Description"></textarea>
            <input id="new-product-price" placeholder="Product Price" ng-model="productPrice">
            <input type="submit" value="Create Product">
        </form>
    </header>
    <section id="main" ng-show="products.length" ng-cloak>
        <ul id="product-list">
            <li ng-repeat="product in products | filter:statusFilter" ng-class="{ editing: product == editedProduct}">
                <div class="view">
                    <label ng-dblclick="editProduct(product)">{{product.name}}</label>
                    <label>{{product.price}}</label>
                    <button class="destroy" ng-click="removeProduct(product)"></button>
                </div>
                <form id="edit-product" class="hide" ng-submit="doneEditing(product)">
                    <input class="edit" ng-model="product.name" product-blur="doneEditing(product)" product-focus="product == editedProduct">
                </form>
            </li>
        </ul>
    </section>
    <footer id="footer" ng-show="products.length" ng-cloak>
      <span id="product-count"><strong>{{remainingCount}}</strong>
        <ng-pluralize count="remainingCount" when="{ one: 'item left', other: 'items left' }"></ng-pluralize>
      </span>
        <ul id="filters">
            <li>
                <a ng-class="{selected: location.path() == '/'} " href="#/">All</a>
            </li>
            <li>
                <a ng-class="{selected: location.path() == '/active'}" href="#/active">Active</a>
            </li>
            <li>
                <a ng-class="{selected: location.path() == '/completed'}" href="#/completed">Completed</a>
            </li>
        </ul>
    </footer>
</section>
<footer id="info">
    <p>Double-click to edit a product</p>
    <p>Credits:
        <a href="http://twitter.com/cburgdorf">Christoph Burgdorf</a>,
        <a href="http://ericbidelman.com">Eric Bidelman</a>,
        <a href="http://jacobmumm.com">Jacob Mumm</a>
        <a href="http://igorminar.com">Igor Minar</a> and
        <a href="http://zamboch.blogspot.com">Pavel Savara</a>
    </p>
</footer>
<script type="text/javascript" src="js/libs/jquery.js"></script>
<script src="js/libs/angular.js"></script>
<script src="js/models/Product.js"></script>
<script src="js/controllers/ProductCtrl.js"></script>
<script src="js/services/SummerStorage.js"></script>
<script src="js/directives/ProductFocus.js"></script>
<script src="js/directives/ProductBlur.js"></script>
<script src="js/Application.js"></script>
</body>
</html>